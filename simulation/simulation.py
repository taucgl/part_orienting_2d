"""
Author: Geva Kipper
Submitted for: Algorithms for 3D printing, TAU
Simulates a polygon being oriented by a straight robotic arm. 
To demonstrate that it works for any orientation,
a grid of simulations is shown simultaneously.
"""

import math, sys, random, os
import pygame
from pygame.locals import *
from pygame.color import *
import pymunk
import itertools
import pymunk.pygame_util
from pymunk import Vec2d


## Global parameters
TICKS_BETWEEN_PUSH = 400        # The time to wait between push directions
DISPLAY_SIZE = (1200, 800)      # The size of the window
ROW_SIZE = 2                    # The number of polygon objects in a row
NUM_SIMS = ROW_SIZE * ROW_SIZE  # We will have a square grid of synchronized simulations

# The scale of the objects to fit in the grid
OBJECT_SCALE = min(DISPLAY_SIZE[0], DISPLAY_SIZE[1]) / (2*ROW_SIZE) 
# The length of the pushing segments ('arms')
SEGMENT_LENGTH = min(DISPLAY_SIZE[0], DISPLAY_SIZE[1]) / (ROW_SIZE) - 10

# Used to set constant velocity for the robotic arm
def constant_velocity(speed):
    def velocity_func(body, gravity, damping, dt):
        body.velocity = body.velocity.normalized() * speed
    return velocity_func


def parse_rational(s):
    """
    Parse a rational from a string.
    Supports the format "1.17" as well as "33/47".
    """
    if "/" not in s:
        return float(s)
    p, q = s.split("/")
    return float(p) / float(q)


def poly_from_file(path, scale):
    """
    Reads a polygon from the file 'path' in a simple point list format.
    Scales the polygon to fit in a bounding box of side length 'scale',
    and translates the polygon so that the center of mass is at (0,0).
    """
    vertices = []
    lines = open(path, "r").readlines()
    for line in lines:
        x,y = map(parse_rational, line.split(" "))
        vertices.append((x*scale, y*scale))

    # Scale to fit in bounding square of side length 'scale'
    width = max([v[0] for v in vertices]) - min([v[0] for v in vertices])
    height = max([v[1] for v in vertices]) - min([v[1] for v in vertices])
    scale = float(scale)
    scale_factor = min(scale / width, scale / height)
    vertices = [(v[0] * scale_factor, v[1] * scale_factor) for v in vertices]

    # Translate vertices to move center of mass to origin
    # This function is not exported by pymunk so we call it manually
    com = Vec2d._fromcffi(pymunk.cp.cpCentroidForPoly(len(vertices), vertices))
    vertices = map(lambda v: (v[0] - com[0], v[1] - com[1]), vertices)
    return vertices

    
def plan_from_file(path):
    """
    Reads a push-plan from the file 'path' in the direction list format,
    as used by the part_orient program.
    """
    plan = []
    lines = open(path, "r").readlines()
    for push in lines:
        x,y = map(parse_rational, push.split(" "))
        plan.append(Vec2d((x, y)))
        
    return plan
    

def add_moving_segment(image_center, normal, length, distance, speed):
    """
    Adds a moving segment ('robotic arm') to the world space.
    'image_center' should be the center of the pushed object.
    'normal' determines the direction (slope) of the segment.
    'length' determines the length of the segment.
    'distance' determines the starting distance between the segment and the center of the object.
    'speed' determines the (constant) speed at which the segment moves.
    """
    # Set large mass to prevent segment from being swayed
    segment_mass = 10000 
    width = 3.0
    normal = normal.normalized()
    # Place segment far from the object
    mid = image_center + normal * distance
    direction = normal.perpendicular_normal()
    # Calculate endpoints and intertia
    beg = mid - direction * (length / 2)
    end = mid + direction * (length / 2)
    inertia = pymunk.moment_for_segment(segment_mass, beg, end, width)    
    segment_body = pymunk.Body(segment_mass, inertia)
    # Set the velocity direction and size
    segment_body.velocity = -normal
    segment_body.velocity_func = constant_velocity(speed)
    # Create and add segment
    shape = pymunk.Segment(segment_body, beg, end, width)
    space.add(segment_body, shape)
    return shape

    
def add_poly(path, center):
    """
    Adds a polygon object to the world space, with a random orientation.
    'poly' is the list of polygon vertices.
    'center' is the (x,y) where the polygon center of mass should be placed
    """
    # Small mass to be easily pushed around
    mass = 20
    vertices = poly
    inertia = pymunk.moment_for_poly(mass, vertices)
    body = pymunk.Body(mass, inertia)
    body.position = center
    # Set random orientation
    body.angle = random.random()*2*math.pi
    shape = pymunk.Poly(body, vertices)
    # Setup physics for pushing
    shape.elasticity = 0.0
    shape.friction = 1.0
    space.add(body, shape)
    # Add joint to resist moving (friction with table)
    joint = pymunk.PivotJoint(space.static_body, body, (0,0), (0,0))
    joint.max_bias = 0
    joint.max_force = 1000
    space.add(joint)
    # Add gear to allow rotation
    gear = pymunk.GearJoint(space.static_body, body, 0, 1)
    gear.max_bias = 0
    gear.max_force = 5000
    space.add(gear)
    return shape

    
def generate_centers(row_size, width, height):
    """
    Generates the list of center points for all polygon objects.
    'row_size' is the number of objects in a row / column.
    'width' is the width of the window.
    'height' is the height of the window.
    """
    xs = [(width*i)/row_size + (width/row_size)/2 for i in xrange(row_size)]
    ys = [(height*i)/row_size + (height/row_size)/2 for i in xrange(row_size)]
    return list(itertools.product(xs,ys))

def main(poly, plan):
    """
    Runs the simulation with the given polygon and push-plan.
    """
    print "Simulating push-plan with length", len(plan)
    pygame.init()
    screen = pygame.display.set_mode(DISPLAY_SIZE)
    clock = pygame.time.Clock()
    running = True

    draw_options = pymunk.pygame_util.DrawOptions(screen)
    
    # Physics stuff
    global space
    space = pymunk.Space()
    space.gravity = (0.0, 0.0)    # No gravity, laying on a table
    static_body = space.static_body

    # Create list of polygon center points
    centers = generate_centers(ROW_SIZE, DISPLAY_SIZE[0], DISPLAY_SIZE[1])
    # Lists of simulation objects (polygons and segments)
    objects = []
    segments = []
    
    # Index of the current push direction in the plan, incremented every few seconds
    push_index = 0
    ticks_to_next_push = TICKS_BETWEEN_PUSH
    # Choose push direction by plan
    normal = plan[push_index]

    # Generate polygons and segments
    for i in xrange(NUM_SIMS):
        segments.append(add_moving_segment(centers[i], normal, length=SEGMENT_LENGTH, distance=OBJECT_SCALE, speed=60))
        objects.append(add_poly(poly, center=centers[i]))

    while running:
        # Handle key press and 'x' press
        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                running = False
            elif event.type == KEYDOWN and event.key == K_p:
                pygame.image.save(screen, "orient_poly.png")
        
        # Clear screens
        screen.fill(THECOLORS["white"])

        ticks_to_next_push -= 1
        if ticks_to_next_push <= 0: # Advance to next push direction
            ticks_to_next_push = TICKS_BETWEEN_PUSH
            # Remove old segments
            for seg in segments:
                space.remove(seg)
            segments = []
            # Reposition objects at centers
            for i in xrange(NUM_SIMS):
                objects[i].body.position = centers[i]
                objects[i].body.velocity = (0,0)
            # Insert new segments
            push_index += 1
            if push_index < len(plan): 
                # Choose next push direction by the plan
                normal = plan[push_index]
                for i in xrange(NUM_SIMS):
                    segments.append(add_moving_segment(centers[i], normal, length=SEGMENT_LENGTH, distance=OBJECT_SCALE, speed=60))

        # Draw space
        space.debug_draw(draw_options)

        # Update physics
        dt = 1.0/40.0
        for x in range(1):
            space.step(dt)
        
        # Flip screen
        pygame.display.flip()
        clock.tick(120)
        pygame.display.set_caption("fps: " + str(clock.get_fps()))
        

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print "Usage: python %s <poly_path> [plan_path]"
        exit(1)
    poly_path = sys.argv[1] 
    # Read polygon from file and scale it
    poly = poly_from_file(poly_path, OBJECT_SCALE)    
    if len(sys.argv) == 3:
        plan_path = sys.argv[2]
        # Read push-plan from file
        plan = plan_from_file(plan_path)
    else:     # If no plan is given, we will simulate a single random push
        plan = [pymunk.Vec2d(random.random() * 2 - 1, random.random() * 2 - 1)]
        
    main(poly, plan)
