/*
 * part_orient.h
 *
 *  Created on: Aug 25, 2017
 *      Author: gevak
 */

#pragma once

#include "Types.h"
#include "Interval.h"

namespace orient {
ostream& operator<<(ostream& os, const Polygon_2& p);
/**
 * Write a vector to output stream.
 */
template<typename ElemType>
ostream& operator<<(ostream& os, const vector<ElemType> vec) {
	cout << "Vector with " << vec.size() << " elements:" << endl;
	for (ElemType e : vec) {
		os << e << endl;
	}
	return os;
}

bool poly_from_file(const string& path, Polygon_2& result);
void plan_to_file(vector<Direction_2> plan, string path);
Point_2 polygon_centroid(const Polygon_2& poly);
void gaussian_map(const Polygon_2& hull, SphereArr& result);
void central_map(const Polygon_2& hull, const Point_2& center, SphereArr& result);
void radius_function(const Polygon_2& poly, SphereArr& result);
void push_function(const Polygon_2& poly, PushArr& result);
void naive_push_plan(const Polygon_2& poly, vector<Direction_2>& result);
void shortest_plan_from_graph(const vector<Interval>& intervals, const IntervalGraphType& g,
							size_t sourceIndex, vector<Direction_2>& result);
void shortest_push_plan(const Polygon_2& poly, vector<Direction_2>& result);
void shortest_push_plan_improved(const Polygon_2& poly, vector<Direction_2>& result);

} // namespace orient
