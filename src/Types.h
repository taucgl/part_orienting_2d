#pragma once

#include "Angle_2.h"
#include "SphereArrangement_1.h"

#include <cerrno>
#include <iostream>
#include <istream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <deque>

#include <boost/range/irange.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/pending/indirect_cmp.hpp>

#include <CGAL/number_utils.h>
#include <CGAL/Gmpq.h>
#include <CGAL/Cartesian.h>
#include <CGAL/circulator.h>
#include <CGAL/Aff_transformation_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/convex_hull_2.h>

namespace orient {


using std::vector;
using std::deque;
using std::cout;
using std::endl;
using std::string;
using std::ofstream;
using std::ostream;
using std::ifstream;

// Basic types
typedef CGAL::Gmpq                                          Number_type;
typedef CGAL::Cartesian<Number_type>                        K;
typedef K::RT 												RT;
typedef K::FT 												FT;
typedef K::Point_2 											Point_2;
typedef K::Line_2											Line_2;
typedef K::Vector_2											Vector_2;
typedef K::Vector_3											Vector_3;
typedef K::Direction_2										Direction_2;
typedef K::Segment_2                        				Segment_2;
typedef CGAL::Polygon_2<K>                     				Polygon_2;
typedef Polygon_2::Vertex_circulator                   		PolygonCirculator;
typedef CGAL::Aff_transformation_2<K> 						Transformation;

// SphereArrangement types
// An enum for the different vertex types
enum SphereVertexType { MAX, MIN, CENTRAL_MAP, GAUSSIAN_MAP };

/**
 * Data type for the basic sphere arrangements (gaussian map, central map).
 */
struct SphereData {
	SphereVertexType type;
	PolygonCirculator originVertex; // The corresponding vertex from the original polygon

	SphereData(SphereVertexType type, PolygonCirculator originVertex) : type(type), originVertex(originVertex) { } ;
	SphereData(const SphereData& other) : type(other.type), originVertex(other.originVertex) { } ;
};

ostream& operator<<(ostream& os, const SphereData& data) {
	os << data.type << "(" << (*(data.originVertex)) << ", " << (*(data.originVertex + 1)) << ")";
	return os;
}

typedef CGAL_Ext::Vertex<Direction_2, SphereData> 			SphereVertex;
typedef CGAL_Ext::SphereArrangement_1<SphereVertex>			SphereArr;

typedef CGAL_Ext::Vertex<Direction_2, Direction_2>			PushStep;
typedef CGAL_Ext::SphereArrangement_1<PushStep>				PushArr;

typedef CGAL_Ext::Angle_2<K>			 					Angle_2;

// boost::graph
typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::bidirectionalS > 	IntervalGraphType;
typedef boost::graph_traits < IntervalGraphType >::vertex_descriptor 				IntervalVertex;
typedef boost::graph_traits < IntervalGraphType >::vertices_size_type 				Size;


} // namespace orient


