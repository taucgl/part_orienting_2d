#include "part_orient.h"
#include "Types.h"
#include "Interval.h"

#include <algorithm>
#include <stdlib.h>

namespace orient {

/**
 * Write a polygon to an output stream.
 */
ostream& operator<<(ostream& os, const Polygon_2& p) {
	os << "Polygon_2 with " << p.size() << " vertices:" << endl;
	auto vcBeg = p.vertices_circulator();
	auto vc = vcBeg;
	do {
		os << vc->x() << " " << vc->y() << endl;
		vc++;
	} while (vc != vcBeg);
	return os;
}


/**
 * Read a polygon from a file located at 'path' into 'result'.
 */
bool poly_from_file(const string& path, Polygon_2& result) {
	try {
		ifstream inFile(path);
		std::istream_iterator<Point_2> in_start(inFile);
		std::istream_iterator<Point_2> in_end;
		std::copy(in_start, in_end, std::back_inserter(result));
	} catch (...) {
		return false; // Error
	}
	return true;
}

/**
 * Save a push plan (vector of push directions) to a file at 'path'.
 * This is saved in a format that is readable by the Python simulation.
 */
void plan_to_file(vector<Direction_2> plan, string path) {
	ofstream of(path);
	for (auto p : plan) {
		of << p << endl;
	}
	of.close();
}

/**
 * Determines the centroid of the given 2d simple polygon using the method described here:
 * https://en.wikipedia.org/wiki/Centroid#Centroid_of_a_polygon
 * This operation takes O(n) time.
 */
Point_2 polygon_centroid(const Polygon_2& poly) {
	Point_2 p1, p2;
	RT cx, cy;
	auto eiBegin = poly.edges_circulator();
	auto ei = eiBegin;
	do {
		p1 = ei->start();
		p2 = ei->end();
		cx += (p1.x() + p2.x()) * (p1.x() * p2.y() - p2.x() * p1.y());
		cy += (p1.y() + p2.y()) * (p1.x() * p2.y() - p2.x() * p1.y());
		ei++;
	} while (ei != eiBegin);

	FT area_6 = 6 * poly.area();
	Point_2 center = Point_2(cx / area_6, cy / area_6);
	return center;
}

/**
 * Constructs the gaussian map for the given convex polygon 'hull' with given center of mass 'center',
 * as a spherical arrangement.
 * The gaussian map maps an orientation 't' to the first feature of 'hull' that will be hit
 * by a line whose normal angle is 't' being dragged from infinity (usually a vertex, edges are singular points).
 * The construction takes O(n) time.
 */
void gaussian_map(const Polygon_2& hull, SphereArr& result) {
	vector < SphereVertex > edgeNormals;
	auto viBeg = hull.vertices_circulator();
	auto vi = viBeg;
	do {
		auto ei = Segment_2(*vi, *(vi+1));
		Direction_2 normal = ei.direction().perpendicular(CGAL::CLOCKWISE);
		SphereData vData(SphereVertexType::GAUSSIAN_MAP, vi);
		SphereVertex v(normal, vData);
		edgeNormals.push_back(v);
		vi++;

	} while (vi != viBeg);
	result = SphereArr(edgeNormals.begin(), edgeNormals.end());
}

/**
 * Constructs the central map for the given convex polygon 'hull' with given center of mass 'center',
 * as a spherical arrangement.
 * The central map maps an orientation 't' to the feature of 'hull' that is directly below 'center'
 * when hull is rotated at angle 't' (usually an edge, vertices are singular points).
 * The construction takes O(n) time.
 */
void central_map(const Polygon_2& hull, const Point_2& center,
		SphereArr& result) {
	vector < SphereVertex > directions;
	auto viBeg = hull.vertices_circulator();
	auto vi = viBeg;
	do {
		Direction_2 dir = Direction_2(Vector_2(center, *vi));
		SphereData vData(SphereVertexType::CENTRAL_MAP, vi);
		SphereVertex v(dir, vData);
		directions.push_back(v);
		vi++;
	} while (vi != viBeg);
	result = SphereArr(directions.begin(), directions.end());
}

/**
 * Constructs the min and max points of the radius function as a spherical arrangement.
 * This operation takes O(n) time for convex polygons and O(n log n) for non-convex polygons.
 * For a non-convex polygon, we first find its convex hull in O(n logn) time.
 * The rest is done by constructing the gaussian map in O(n) time,
 * constructing a map of vertex directions ('central map') in O(n) time,
 * and overlaying them in O(n) time using a 'merge' algorithm.
 * The radius function is then simply computed from the overlay in linear time.
 */
void radius_function(const Polygon_2& poly, SphereArr& result) {
	// Calculate polygon center of mass
	Point_2 center = polygon_centroid(poly);
	// Calculate convex hull
	Polygon_2 hull;
	CGAL::convex_hull_2(poly.vertices_begin(), poly.vertices_end(),
			std::back_inserter(hull));

	// Construct gaussian map and central map
	SphereArr gaussMap;
	gaussian_map(hull, gaussMap);
	SphereArr centerMap;
	central_map(hull, center, centerMap);
	// Construct overlay
	SphereArr overlayMap;
	overlay(gaussMap, centerMap, overlayMap);

	// Sweep the overlay, maintaining last vertices of each type
	PolygonCirculator lastDir = centerMap.last()->data.originVertex;
	PolygonCirculator lastGauss = gaussMap.last()->data.originVertex + 1;
	auto vcBeg = overlayMap.vertex_circulator();
	auto vc = vcBeg;
	// Notice that we have to start with the first to maintain order
	do {
		switch (vc->data.type) {
		case CENTRAL_MAP: // Vertex direction
			// Check if dir(v) is in range(v)
			// That is equivalent to the last gauss vertex being v
			if (vc->data.originVertex == lastGauss) {
				SphereVertex v(vc->dir,
						SphereData(SphereVertexType::MAX, vc->data.originVertex));
				result.append_sorted(v);
			}
			lastDir = vc->data.originVertex;
			break;
		case GAUSSIAN_MAP: // Edge normal
			// Check if n(e) is between dir(v1) and dir(v2)
			// That is equivalent to the last dir vertex being v1
			if (vc->data.originVertex == lastDir) {
				SphereVertex v(vc->dir,
						SphereData(SphereVertexType::MIN, vc->data.originVertex));
				result.append_sorted(v);
			}
			lastGauss = vc->data.originVertex + 1;
			break;
		default:
			CGAL_error_msg("Unexpected vertex type");
		}
		vc++;
	} while (vc != vcBeg);
}

/**
 * Constructs the push function for given general polygon as a spherical arrangement.
 * This operation takes O(n) time for convex polygons and O(n log n) for non-convex polygons.
 * It is done by constructing the radius function of 'poly' (with the time-complexity specified above).
 * The push function is then constant within any two consecutive local-maxima of the radius function,
 * with its value matching the local-minimum between them,
 * and it can thus be calculated in linear time from the radius function.
 */
void push_function(const Polygon_2& poly, PushArr& result) {
	SphereArr radiusArr;
	radius_function(poly, radiusArr);
	auto vcBeg = radiusArr.vertex_circulator();
	auto vc = vcBeg;
	do {
		if (vc->data.type == SphereVertexType::MAX) {
			auto nextMin = vc->next();
			auto nextMax = vc->next()->next();
			CGAL_assertion(nextMin->data.type == SphereVertexType::MIN);
			CGAL_assertion(nextMax->data.type == SphereVertexType::MAX);
			PushStep ps(vc->dir, nextMin->dir);
			result.append_sorted(ps);
		}
		vc++;
	} while (vc != vcBeg);
}

/**
 * Finds a push plan that will orient 'poly' consisting of at most 'n' steps,
 * where 'n' is the number of edges in 'poly'.
 * This operation takes O(n) time for convex polygons and O(n log n) for non-convex polygons.
 * This is achieved by constructing the push function and then finding the
 * largest and 2nd-largest 'half-angles' in O(n) time, as detailed in the paper of Chen and Ierardi:
 * https://link.springer.com/article/10.1007/BF01192046
 */
void naive_push_plan(const Polygon_2& poly, vector<Direction_2>& result) {
	PushArr pushMap;
	push_function(poly, pushMap);
	vector<Angle_2> angles;
	angles.reserve(pushMap.num_vertices());
	// Construct list of "clockwise" half-angles
	auto vcBeg = pushMap.vertex_circulator();
	auto vc = vcBeg;
	do {
		// We construct a half-angle: from the normal of the pushed edge to the next vertex direction
		angles.push_back(Angle_2(vc->data, vc->next()->dir));
		vc++;
	} while (vc != vcBeg);
	CGAL_assertion_msg(pushMap.num_vertices() > 1, "Degenerate push function");
	// Find largest and second-largest angles by partitioning the array
	std::nth_element(angles.begin(), angles.begin() + 1, angles.end(),
			std::greater<Angle_2>());
	Angle_2 max = angles[0];
	Angle_2 second = angles[1];
	CGAL_assertion_msg(max > second,
			"Largest angle is non-unique, currently unsupported");

	Transformation resRotate = second.get_rotation_between(max);
	Direction_2 accum(1, 0);
	for (int i = 0; i < pushMap.num_vertices(); i++) {
		result.push_back(accum);
		accum = resRotate(accum);
	}
}

/**
 * Constructs the shortest push-plan from the given interval graph.
 * The shortest plan corresponds to the shortest path from the source vertex
 * to any singleton in the directed interval graph.
 * It takes O(|E| + |V|) time, determined by the complexity of the graph.
 * Boost graph library (BGL) is used for the BFS graph algorithm.
 */
void shortest_plan_from_graph(const vector<Interval>& intervals, const IntervalGraphType& g,
							size_t sourceIndex, vector<Direction_2>& result) {
	using namespace boost;
	int N = intervals.size();
	integer_range<int> range(0, N);
	// a vector to hold the distance property for each vertex
	vector<Size> d(N);
	// a vector to hold the predecessor property for each vertex
	vector<IntervalVertex> p(N);
	IntervalVertex source = vertex(sourceIndex, g);
	std::fill_n(&d[0], N, N);
	d[source] = 0;
	p[source] = source;
	breadth_first_search(g, source,
			visitor(
					make_bfs_visitor(
							std::make_pair(
									record_distances(&d[0], on_tree_edge()),
									boost::record_predecessors(&p[0],
											on_tree_edge())))));

	int minSingleton = N;
	size_t minIndex = -1;
	// Go over all distances
	for (int i : range) {
		Angle_2 angle(intervals[i].first->data, intervals[i].second->data);
		// Check if this is a singleton
		if (i != sourceIndex && angle.is_size_zero()) {
			if (d[i] < minSingleton) {
				minSingleton = d[i];
				minIndex = i;
			}
		}
	}
	CGAL_assertion_msg(minIndex != -1, "Interval graph is not connected");
	size_t i = minIndex;
	// Insert pushes in reverse order
	while (p[i] != sourceIndex) { // Until we reach the max interval, can also check i != source
		Direction_2 pushDir = get_interval_push(intervals[p[i]], intervals[i]);
		result.push_back(pushDir);
		i = p[i];
	}
	result.push_back(Direction_2(0, -1)); // First push doesn't matter (nothing known)
	std::reverse(result.begin(), result.end());

	// All directions in the list are relative to the previous - transform them to absolute
	for (int i = 1; i < result.size(); i++) {
		result[i] = CGAL_Ext::rotate_ccw(result[i], result[i - 1]);
	}
}

/**
 * Finds a push plan of minimal length that will orient 'poly' in O(n^4) time.
 * This is done by running BFS on the graph whose vertices are intervals of stable orientations,
 * as detailed in the paper of Goldberg et al.:
 * http://goldberg.berkeley.edu/pubs/geometry-part-feeding.pdf
 * Boost graph library (BGL) is used for graph algorithms.
 */
void shortest_push_plan(const Polygon_2& poly, vector<Direction_2>& result) {
	PushArr pushMap;
	push_function(poly, pushMap);
	vector<Interval> intervals;
	auto vcBeg = pushMap.vertex_circulator();
	size_t sourceIndex = 0;
	intervals.push_back(Interval(vcBeg, vcBeg)); // Dummy vertex for 'full angle', used as source vertex
	// Insert all n^2 intervals as vertices of the interval graph
	append_stable_intervals(pushMap, intervals);
	size_t N = intervals.size();
	boost::integer_range<int> range(0, N);
	IntervalGraphType g(N);
	// Add all existing edges between two connected intervals
	// This takes O(n^4) times, going over each pair of intervals
	for (int i1 : range) {
		for (int i2 : range) {
			if (i1 == sourceIndex && is_full_angle(intervals[i2])) { // Connect source vertex to all full arcs with determined 'start point'
				add_edge(i1, i2, g);
			} else if (i2 != sourceIndex && i1 != sourceIndex
					&& connected_single_push(intervals[i1], intervals[i2])) {
				add_edge(i1, i2, g);
			}
		}
	}
	// We have constructed the full interval graph
	// We now find the shortest path and construct an optimal push-plan from it
	shortest_plan_from_graph(intervals, g, sourceIndex, result);
}

/**
 * Finds a push plan of minimal length that will orient 'poly' in O(n^2 log n) time.
 * This is an improved version of shortest_push_plan, where we
 * only construct a partial (but sufficient) subgraph of the interval graph.
 */
void shortest_push_plan_improved(const Polygon_2& poly, vector<Direction_2>& result) {
	PushArr pushMap;
	push_function(poly, pushMap);
	vector<Interval> intervals;
	auto vcBeg = pushMap.vertex_circulator();
	// Insert all n^2 intervals as vertices of the interval graph
	append_stable_intervals(pushMap, intervals);

	// Sort the intervals by fit angle
	std::sort(intervals.begin(), intervals.end(), less_than_fit_angle);
	intervals.push_back(Interval(vcBeg, vcBeg)); // Dummy vertex for 'full angle', used as source vertex
	size_t N = intervals.size();
	size_t sourceIndex = N - 1;
	boost::integer_range<int> range(0, N-1); // Range of all regular vertices
	// For each interval I, obtain the smallest interval (by known-angle) with fit-angle at least like I
	vector<size_t> smallestKnown(N-1);
	size_t minIndex = N-2;
	Angle_2 minAngle = known_angle(intervals[minIndex]);
	for (int i=N-2; i>=0; i--) {
		Angle_2 knownAngle = known_angle(intervals[i]);
		if (knownAngle < minAngle) {
			minIndex = i;
			minAngle = knownAngle;
		}
		smallestKnown[i] = minIndex;
	}

	// Construct the O(n^2) edges of the graph
	// Connect source vertex to all full angle intervals
	IntervalGraphType g(N);
	for (int i2 : range) {
		if (is_full_angle(intervals[i2])) {
			add_edge(sourceIndex, i2, g);
		}
	}

	// Add for each interval I1 an edge to the smallest interval that I1 can fit in
	// This takes O(n^2 log n) time, using binary search once on each interval
	for (int i1 : range) {
		// Find smallest fit index using binary search
		Angle_2 knownAngle = known_angle(intervals[i1]);
		size_t i2 = std::lower_bound(intervals.begin(), intervals.end()-1, // Ignore the dummy vertex
								knownAngle, less_than_fit_angle) - intervals.begin();
		i2 = smallestKnown[i2]; // We take the smallest interval (by known-angle) that I1 fits in
		CGAL_assertion_msg(connected_single_push(intervals[i1], intervals[i2]), "I1 should fit inside I2 by construction");
		add_edge(i1, i2, g);
	}

	// We have constructed the full interval graph
	// We now find the shortest path and construct an optimal push-plan from it
	shortest_plan_from_graph(intervals, g, sourceIndex, result);
}

} // namespace orient

using namespace orient;
int main(int argc, char* argv[]) {
	Polygon_2 poly;
	if (argc != 2) {
		cout << "Usage: " << argv[0] << " <polygon_path>" << endl << endl;
		return 1;
	}
	cout << "Working on file: \"" << argv[1] << "\"" << endl;
	if (!poly_from_file(argv[1], poly) || poly.is_empty()) {
		cout << "Error reading polygon from input file \"" << argv[1]
				<< "\". Exiting." << endl;
		return 1;
	}

	PushArr pushMap;
	push_function(poly, pushMap);
	cout << endl << "Push Function - " << pushMap << endl;

	vector<Direction_2> naivePlan;
	naive_push_plan(poly, naivePlan);
	string naivePlanPath = string(argv[1]) + string(".naive.plan");
	cout << "Saving naive plan to " << naivePlanPath << endl;
	plan_to_file(naivePlan, naivePlanPath);
	cout << "Naive Push Plan - " << naivePlan << endl;


	vector<Direction_2> optimalPlan;
	shortest_push_plan(poly, optimalPlan);
	string optimalPlanPath = string(argv[1]) + string(".optimal.plan");
	cout << "Saving optimal plan to " << optimalPlanPath << endl;
	plan_to_file(optimalPlan, optimalPlanPath);
	cout << "Optimal Push Plan - " << optimalPlan << endl;

	// This is unused, it is just here to show how to call shortest_push_plan_improved
	vector<Direction_2> optimalPlan2;
	shortest_push_plan_improved(poly, optimalPlan2);
	return 0;

}

