/*
 * interval.h
 *
 *  Created on: Aug 25, 2017
 *  	Author: gevak
 *
 *  Contains methods for working with intervals of stable angles.
 *  The Interval type is used to represent an 'interval' between two stable orientations of the polygon.
 */

#pragma once

#include "Types.h"

namespace orient {

/**
 * This type is used to represent an 'interval' between two stable orientations of the polygon.
 */
typedef std::pair<PushStep::Handle, PushStep::Handle>	 	Interval;

bool is_full_angle(const Interval& i);
Angle_2 known_angle(const Interval& i);
Angle_2 fit_angle(const Interval& i);
bool connected_single_push(const Interval& i1, const Interval& i2);
Direction_2 get_interval_push(const Interval& i1, const Interval& i2);
void append_stable_intervals(const PushArr& pushMap, vector<Interval>& result);

/**
 * Used to compare intervals by fit-angle,
 * or compare an intervals fit angle to a given angle.
 */
struct less_than_fit_angle_type {
	bool operator()(const Interval& i1, const Interval& i2) {
		if (is_full_angle(i1)) { // This is required to have strict order
			return false;
		}
		if (is_full_angle(i2)) { // Special case, since fitAngle2 will seem to be a singleton
			return true;
		}
		return fit_angle(i1) < fit_angle(i2);
	}

	bool operator()(const Interval& i2, const Angle_2& a) {
		if (is_full_angle(i2)) { // Special case, since fitAngle2 will seem to be a singleton
			return false;
		}
		return fit_angle(i2) < a;
	}
} less_than_fit_angle;

/*
 * Returns true iff the interval 'i' corresponds to a 2*pi arc containing all possible stable positions.
 * This is a simple O(1) operation.
 */
bool is_full_angle(const Interval& i) {
	return i.second->next() == i.first;
}

/**
 * Returns an angle Angle(d1, d2) such that if the polygon is standing on one of the edges
 * in the interval 'i', then it is between orientations d1 and d2.
 * d1 and d2 will simply be the directions of the first and last stable angles in the interval.
 */
Angle_2 known_angle(const Interval& i) {
	return Angle_2(i.first->data, i.second->data);
}

/**
 * Returns an angle Angle(d1, d2) such that for the polygon to stabilize on one of the edges
 * in the interval 'i', it can be pushed in any of the directions between d1 and d2.
 * d1 will be the direction of the local maximum just before the first stable angle,
 * and d2 the direction of the local maximum right after the last stable angle.
 */
Angle_2 fit_angle(const Interval& i) {
	return Angle_2(i.first->dir, i.second->next()->dir);
}

/**
 * Returns true if there exists a push direction 'd' that will send any direction contained in 'a1' to within 'a2'.
 * This is a simple O(1) operation.
 */
bool connected_single_push(const Interval& i1, const Interval& i2) {
	// Check if i2's fit angle is the full interval
	// This is a special case because it will appear to be a zero angle (we can't represent a full 2PI angle here)
	if (is_full_angle(i2)) {
		return true;
	}

	// We know we are between two stable orientations
	// We need to fit in the range that 'collapses' to i2, that is between two vertices of the push map
	// Notice that this only depends on the relation of the sizes, which is the basis for the O(n^2) algorithm
	return (fit_angle(i2) >= known_angle(i1));
}

/**
 * Assuming the polygon is currently pushed from direction 's',
 * and it has stabilized on one of the stable angles in the interval 'i1',
 * returns a direction 'd' such that when we push from direction 's'+'d' the
 * polygon will rest on one of the stable angles in interval 'i2'.
 * If there is no suitable push direction, an error is thrown.
 * This is a simple O(1) operation.
 *
 * This could be implemented more cleverly, to avoid pushing close to local maxima,
 * which would prevent some simulation failures.
 */
Direction_2 get_interval_push(const Interval& i1, const Interval& i2) {
	Angle_2 knownAngle = known_angle(i1);
	// We need to fit in the range that 'collapses' to i2, that is between two vertices of the push map
	Angle_2 fitAngle = fit_angle(i2);
	CGAL_assertion_msg(fitAngle >= knownAngle, "Expected angles to fit");
	// If a push direction exists, it should push knownAngle.d after fitAngle.d, and knownAngle.e before fitAngle.e
	// That is, it should be between fitAngle.d - knownAngle.d and fitAngle.e - knownAngle.e
	Angle_2 goodRange(CGAL_Ext::rotate_cw(fitAngle.d, knownAngle.d),
			CGAL_Ext::rotate_cw(fitAngle.e, knownAngle.e));
	return goodRange.get_direction_in_range();
}

/**
 * Appends to 'result' all n^2 stable intervals between two stable orientations
 * in the given push function 'pushMap'.
 */
void append_stable_intervals(const PushArr& pushMap, vector<Interval>& result) {
	auto vcBeg = pushMap.vertex_circulator();
	auto vc = vcBeg;
	do {
		auto vc2Beg = pushMap.vertex_circulator();
		auto vc2 = vc2Beg;
		do {
			result.push_back(Interval(vc, vc2));
			vc2++;
		} while (vc2 != vc2Beg);
		vc++;
	} while (vc != vcBeg);
}

} // namespace orient
