/*
 * ExactAngle.h
 *
 * Defines an 'Angle_2' class, for working with exact angles in 2d.
 * This file also contains some utility methods for Direction_2 objects.
 *
 *  Created on: Jul 13, 2017
 *      Author: gevak
 *
 *
 */

#pragma once

#include <iostream>

#include <CGAL/Aff_transformation_2.h>
#include <CGAL/enum.h>
#include <CGAL/Kernel/global_functions.h>
#include <CGAL/rational_rotation.h>

namespace CGAL_Ext {

using CGAL::Comparison_result;
using CGAL::Sign;
using CGAL::Orientation;

// Useful methods for Direction_2 objects

/**
 * Rotate 'dir' in clockwise direction by 'rot'.
 */
template <typename Kernel>
CGAL::Direction_2<Kernel> rotate_cw(CGAL::Direction_2<Kernel> dir, CGAL::Direction_2<Kernel> rot) {
	auto res = CGAL::Direction_2<Kernel>(rot.dx()*dir.dx() + rot.dy()*dir.dy(),
										 -rot.dy()*dir.dx() + rot.dx()*dir.dy());
	return res;
}

/**
 * Rotate 'dir' in counterclockwise direction by 'rot'.
 */
template <typename Kernel>
CGAL::Direction_2<Kernel> rotate_ccw(CGAL::Direction_2<Kernel> dir, CGAL::Direction_2<Kernel> rot) {
	auto res = CGAL::Direction_2<Kernel>(rot.dx()*dir.dx() - rot.dy()*dir.dy(),
										rot.dy()*dir.dx() + rot.dx()*dir.dy());
	return res;
}

template <typename Kernel>
inline typename Kernel::RT dot_product(const CGAL::Direction_2<Kernel>& d1, const CGAL::Direction_2<Kernel>& d2) {
	return d1.dx() * d2.dx() + d1.dy() * d2.dy();
}

/**
 * Returns a rotation transformation corresponding to the given direction 'd'.
 */
template <typename Kernel>
CGAL::Aff_transformation_2<Kernel> to_rotation(CGAL::Direction_2<Kernel> d) {
	return CGAL::Aff_transformation_2<Kernel>(d.dx(), -d.dy(), d.dy(), d.dx(), 1);
}

/**
 * Returns the squared cosine of 'dir' with the positive 'x' axis direction.
 */
template <typename Kernel>
typename Kernel::FT squared_cos(const CGAL::Direction_2<Kernel>& dir) {
	return (dir.x * dir.x) / dot_prod(dir, dir);
}


/**
 * Convenience method for getting a normalized rational approximation of a direction vector 'd'.
 */
template <typename Kernel>
CGAL::Direction_2<Kernel> approximate(const CGAL::Vector_2<Kernel>& d, const typename Kernel::FT& epsilon) {
	typedef typename Kernel::FT			FT;

	FT sin_num, cos_num, denom;
	CGAL::rational_rotation_approximation<FT>(d.x(), d.y(), sin_num, cos_num, denom, epsilon.numerator(), epsilon.denominator());
	FT sin = sin_num / denom;
	FT cos = cos_num / denom;
	return CGAL::Direction_2<Kernel>(cos, sin);
}

/**
 * This class is used to represent the angle created by two direction vectors.
 * This allows to work with precise angles instead of saving an approximation of the (irrational) angle size.
 * The angles are ordered by size - where an angle from 'd' to 'e' is assigned a size between 0 and 2*PI,
 * NOT including 2*PI.
 */
template <typename Kernel>
class Angle_2 {
public:
	typedef Kernel								K;
	typedef typename K::Vector_2 				Vector_2;
	typedef typename K::Direction_2 			Direction_2;
	typedef typename K::Aff_transformation_2	Transformation;
	typedef typename K::RT 						RT;
	typedef typename K::FT 						FT;

	// The angle is directed from 'd' to 'e'
	Direction_2 d;
	Direction_2 e;
	Angle_2(const Direction_2& d, const Direction_2& e) : d(d), e(e) {};

	// Order operators for angles
	bool operator> (const Angle_2<Kernel>& a2) const {
		return this->to_direction() > a2.to_direction();
	}

	bool operator< (const Angle_2<Kernel>& a2) const {
		return this->to_direction() < a2.to_direction();
	}

	bool operator<= (const Angle_2<Kernel>& a2) const {
		return this->to_direction() <= a2.to_direction();
	}

	bool operator>= (const Angle_2<Kernel>& a2) const {
		return this->to_direction() >= a2.to_direction();
	}

	bool operator== (const Angle_2<Kernel>& a2) const {
			return this->to_direction() == a2.to_direction();
	}

	bool operator> (const Direction_2& d) const {
		return this->to_direction() > d;
	}

	/**
	 * Returns the squared cosine of this angle.
	 */
	FT squared_cos() const {
		RT dot_prod = dot_product(this->d, this->e);
		RT dot_prod2 = dot_prod * dot_prod;
		RT nd2 = dot_product(this->d, this->d);
		RT ne2 = dot_product(this->e, this->e);
		return dot_prod2 / (nd2 * ne2);
	}

	/**
	 * Returns the sign of the cosine of this angle.
	 */
	Sign cos_sign() const {
		RT dot_prod = dot_product(this->d, this->e);
		return CGAL::sign(dot_prod);
	}

	/*
	 * Compares the size of this angle to PI.
	 */
	bool is_greater_pi() const {
		return this->to_direction() > Direction_2(-1, 0);
	}

	/**
	 * Returns true iff this angle represents a 'singleton'.
	 */
	bool is_size_zero() const {
		return this->to_direction() == Direction_2(1,0);
	}

	/*
	 * Returns a direction whose angle from the x axis is equal in size to this angle.
	 */
	Direction_2 to_direction() const {
		Direction_2 res = rotate_cw(this->e, this->d);
		return res;
	}

	/**
	 * Return some direction in the range of angle.d and angle.e.
	 * There is no guarentee on where inside the range the returned direction will be
	 * (for instance, there is no reason to assume it is the angle bisector).
	 */
	Direction_2 get_direction_in_range() const {
		auto res =  Direction_2((this->d.dx() + this->e.dx()) / 2, (this->d.dy() + this->e.dy()) / 2);
		if (this->is_greater_pi()) {
			res = -res;
		}
		CGAL_assertion_msg(res.counterclockwise_in_between(this->d, this->e), "Bad middle direction");
		return res;
	}

	/**
	 * Returns a rotation transformation whose size is somewhere between the sizes of the given angles.
	 */
	Transformation get_rotation_between(const Angle_2<Kernel>& a2) const {
		Direction_2 d = Angle_2(this->to_direction(), a2.to_direction()).get_direction_in_range();
		return to_rotation(d);
	}

	/**
	 * Returns a direction 'dir' such that cos(dir) differs from cos(mid) by at most epsilon * (cos(e) - cos(d)),
	 * where 'mid' is the midpoint of this angle.
	 */
	Direction_2 get_approximate_middle(FT eps) {
		Direction_2 DIRECTION_ZERO(1,0);
		// Find some direction in between the angles
		// Every approx our angles cos will potentially get closer by epsilon
		// To begin with, we know a^2 - b ^2 = (a+b) (a-b) < 2 (a-b), so a-b > (a^2 - b^2) / 2
		FT delta = eps * (CGAL::abs(squared_cos(this->d) - squared_cos(this->e)) / 16);
		// This means they might get closer by at most eps*(a-b)/8 every approximation
		Direction_2 dApprox = approximate(d, delta);
		Direction_2 eApprox = approximate(e, delta);
		Vector_2 mid = Vector_2(dApprox.dx() + eApprox.dx(), dApprox.dy() + eApprox.dy()) / 2;
		Direction_2 res = approximate(mid, delta);
		return res;
	}


};

/**
 * Write an Angle_2 object to an output stream.
 */
template <typename Kernel>
std::ostream& operator<<(std::ostream& os, const Angle_2<Kernel>& a) {
	return os << a.d << ", " << a.e;
}


} //namespace CGAL_Ext
