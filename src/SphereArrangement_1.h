/*
 * SphereArrangement1.h
 *
 *  Created on: Jun 30, 2017
 *      Author: gevak
 */

#pragma once

#include <cerrno>
#include <iostream>
#include <istream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <deque>
#include <list>
#include <iterator>

#include <CGAL/circulator.h>

namespace CGAL_Ext {

using std::vector;
using std::deque;
using std::list;
using std::endl;
using std::string;
using std::ofstream;
using std::ostream;
using std::ifstream;

/**
 * Declaration of SphereArrangement type, templated by the vertex type.
 */
template <typename VertexType> class SphereArrangement_1;

/**
 * This struct is used to make the container type configurable.
 * It is possible to use any container where iterators are still valid after insertion
 * (so deque and list should work, but vector won't work as it can get reallocated).
 */
template <typename VertexType>
struct VertexContainer {
	typedef deque< VertexType > 		type;
};

/**
 * A class representing a vertex in a spherical arrangement.
 * Any vertex can hold 'topological' data of customizable type, DataType.
 * The Direction type is also configurable.
 * It also contains pointers to the next and previous vertices in the arrangement.
 */
template <typename Direction_2, typename DataType>
class Vertex { // Vertex in 1D Sphere
public:
	// We use templates to make it work with any container type and any vertex type
	friend class SphereArrangement_1< Vertex< Direction_2, DataType > >;
	typedef CGAL::Const_circulator_from_container< typename VertexContainer< Vertex< Direction_2, DataType > >::type >	Handle;
	Direction_2 dir;
	DataType data;
	Vertex(Direction_2 dir, DataType data) : dir(dir), data(data), mPrev(), mNext() { };
	/**
	 * operator< implementation for sorting
	 */
	bool operator<(const Vertex<Direction_2, DataType>& other) const {
		return this->dir < other.dir;
	}
	Handle next() const { return this->mNext; }
	Handle prev() const { return this->mPrev; }
private:
	Handle mPrev;
	Handle mNext;

};

/*
 * Perform overlay of two 1d sphere arrangements with the same vertex data type in O(n) time,
 * and place the result in 'result'.
 * Any vertex in the overlay will maintain its original data.
 */
template <typename VertexType>
void overlay(const SphereArrangement_1<VertexType>& first, const SphereArrangement_1<VertexType>& second, SphereArrangement_1<VertexType>& result) {
	// Simply merge the sorted direction arrays. They are sorted by the direction angle.
	std::merge(first.vertices.begin(), first.vertices.end(),
			second.vertices.begin(), second.vertices.end(), std::back_inserter(result.vertices));
	// Reset next/prev pointers
	result.init_pointers();
}

/**
 * Sorts the elementsin-place in O(n) time,
 * assuming they are already sorted upto cyclic rotation (we refer to this as rot-sorted).
 */
template <typename Container>
void rotate_sort(Container& elems) {
	auto newBeg = std::min_element(elems.begin(), elems.end());
	std::rotate(elems.begin(), newBeg, elems.end());
	CGAL_assertion(is_sorted(elems.begin(), elems.end()));
}


/**
 * Definition of SphereArrangement type, templated by the vertex type.
 * The arrangement basically consists of a container of vertices, each
 */
template <typename VertexType>
	class SphereArrangement_1 {
	private:
		typedef typename VertexContainer<VertexType>::type			VertexContainerType;
		VertexContainerType vertices; // Sorted list of vertices, starting with the smallest clockwise angle

		/**
		 * Straightforward O(n) resetting of the next and prev pointers,
		 * used after methods that modify the list of vertices.
		 */
		void init_pointers() {
			auto circ = this->vertex_circulator();
			for (auto it = vertices.begin(); it != vertices.end(); it++) {
				it->mNext = std::next(circ);
				it->mPrev = std::prev(circ);
				circ++;
			}
		}

	public:
		typedef CGAL::Const_circulator_from_container<VertexContainerType>	VertexCirculator;
		typedef VertexCirculator											VertexHandle;

		/**
		 * Copy constructor.
		 */
		SphereArrangement_1(const SphereArrangement_1<VertexType>& other) : vertices(other.vertices) { }

		/**
		 * Assignment operator.
		 */
		SphereArrangement_1& operator=( const SphereArrangement_1<VertexType>& other) {
			this->vertices = other.vertices;
			return *this;
		}

		/**
		 * Empty constructor for the empty arrangement.
		 */
		SphereArrangement_1() : vertices() {}

		/**
		 * Constructor from 'rot-sorted' iterator of vertices - that is, they are sorted but do no begin at the smallest element,
		 * or in other words they can be obtained from a sorted list by cyclic rotation.
		 */
		template <typename Iterator>
		SphereArrangement_1(Iterator beg, Iterator end) : vertices(beg, end) {
			rotate_sort(this->vertices);
			this->init_pointers();
		}

		virtual ~SphereArrangement_1() { };

		/**
		 * Get a circulator of the vertices (ordered by direction).
		 */
		VertexCirculator vertex_circulator() const {
			return VertexCirculator(&this->vertices);
		}

		/**
		 * Inserts a vertex to the arrangement in O(1) time,
		 * assuming it comes after the last vertex inserted.
		 */
		void append_sorted(VertexType& v) {
			CGAL_assertion(this->vertices.empty() || this->last()->dir <= v.dir);
			if (!this->empty()) {
				v.mPrev = std::prev(this->vertex_circulator());
				v.mNext = this->vertex_circulator();
			}
			this->vertices.push_back(v);
			if (this->num_vertices() > 1) {
				std::prev(this->vertices.end(), 2)->mNext = std::prev(this->vertex_circulator());
				this->vertices.begin()->mPrev = std::prev(this->vertex_circulator());
			}
		}

		/**
		 * Returns the number of vertices in the arrangement.
		 */
		int num_vertices() const { return this->vertices.size(); }

		/**
		 * Returns a handle to the last vertex of the arrangement.
		 */
		VertexHandle last() const {
			return std::prev(this->vertex_circulator());
		}

		/**
		 * Returns true iff this arrangement is empty.
		 */
		bool empty() const { return this->vertices.empty(); }

		/**
		 * Overlay two spherical arrangements in O(n) time.
		 */
		friend void overlay <VertexType>(const SphereArrangement_1<VertexType>& first, const SphereArrangement_1<VertexType>& second, SphereArrangement_1<VertexType>& result);

	};

/**
 * Write a sphere arrangement to an output stream,
 * assuming the vertex data type has operator<< implemented.
 */
template <typename SphereVertexType>
ostream& operator<<(ostream& os, const SphereArrangement_1<SphereVertexType>& arr) {
	os << "SphereArrangement with " << arr.num_vertices() << " vertices:" << endl;
	auto vcBeg = arr.vertex_circulator();
	auto vc = vcBeg;
	do {
		os << vc->dir << " data=[" << vc->data << "]" << endl;
		vc++;
	} while (vc != vcBeg);
	return os;
}

} // namespace CGAL_Ext
