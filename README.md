The project deals with the problem of orienting a given polygon P without the use of sensors. We
focus on a method called oblivious push-plans, which means we push P using a straight arm from
several different directions, and construct this series of directions such that P will always end up in
the same, predetermined orientation, regardless of the orientation in which it is given to us.

The project includes the implementation of several algorithms that construct valid and optimal push-
plans for a given polygon P, as well as a simulation program which can be used to demonstrate how
the push-plan we obtain operates on different orientations of P.

Version 1.

Dependencies
The python simulation requires installing pymunk and pygame, which can be done by:
pip install pymunk
pip install pygame

Running the algorithms
The C++ program, called part_orient, is compiled using CMake. Several randomly generated
examples are provided and they can be used to test the program. All examples provided are convex
due to the simulation limitations described above. After compilation, the code can be executed by:
./part_orient <polygon_path>
For instance:
./part_orient ./examples/pres.cin
It will then write to standard output:
1. The push function of the input polygon, as described earlier.
2. A naive O(n) push-plan that orients the input polygon (“naïve”).
3. An optimal push-plan that orients the input polygon (“optimal”).
The push-plans will also be saved to files, which will in this example be named:
./examples/pres.cin.naive.plan
./examples/pres.cin.optimal.plan

Running the simulation
After generating a push-plan using the part_orient program, we can visualize it by running:
python simulation/simulation.py <polygon_path> <plan_path> For instance:
python simulation/simulation.py ./examples/pres.cin ./examples/pres.cin.optimal.plan
This should display a window with 4 copies of the input polygon being oriented simultaneously by
the provided push-plan.

The C++ code requires having CGAL configured, and it also requires CMake for compilation.

Author: Geva Kipper
